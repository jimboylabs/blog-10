import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/views/**/*.md',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
                mono: ["Roboto Mono", "monospace"],
            },
            typography: (theme) => ({
                DEFAULT: {
                    css: {
                        'code::before': {
                            content: '',
                        },
                        'code::after': {
                            content: '',
                        },
                        pre: {
                            paddingBottom: 0,
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 0,
                        },
                    },
                },
            }),
        },
    },

    plugins: [forms, typography],
};
