<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $title ?? 'Wayan Jimmy'}}</title>
        <!-- Scripts -->
        @if(app()->environment('production'))
        <script defer data-domain="blog.wayanjimmy.xyz" src="https://analytics.rollingsayu.xyz/js/script.outbound-links.js"></script>
        <script>window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }</script>
        @endif
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="max-w-(--breakpoint-md) mx-auto font-mono text-lg text-black bg-white lg:text-xl">
      <header class="px-4 py-2 text-black bg-white lg:horizontal md:vertical">
        <livewire:navigation />
        <div id="search" class="lg:w-1/2 sm:w-full"></div>
      </header>
      <main class="container px-4 py-4 mx-auto">
        {{ $slot }}
      </main>
      <footer class="container px-4 py-2 mx-auto text-sm">
        <div>served by {{ $hostname }} with site version {{ $build }}</div>
        <div class="mt-1">
          <a href="/feed" class="hover:underline hover:border-gray-600 hover:cursor-pointer">rss</a> •
          <a href="https://memos.wayanjimmy.xyz" class="hover:underline hover:border-gray-600 hover:cursor-pointer">memos</a> •
          <a href="https://braindump.fly.dev" class="hover:underline hover:border-gray-600 hover:cursor-pointer">braindump (archived)</a> •
          © {{ date('Y') }} wayanjimmy
        </div>
      </footer>
</html>
