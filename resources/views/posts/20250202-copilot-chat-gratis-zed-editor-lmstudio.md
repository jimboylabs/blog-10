# Alternatif Copilot Chat Gratis di Zed editor dengan Lmstudio

[TOC]

## Latar Belakang

Walaupun [Github
Copilot](https://github.blog/news-insights/product-news/github-copilot-in-vscode-free/)
sudah gratis tapi kalau penggunaan kita melebihi limit harus tunggu lagi sampai
limit-nya di reset. Yang mana limit-nya cukup kecil yaitu hanya 50 message
perbulan, jika pengen tetep hemat alternatif-nya menjalankan LLM di local
machine. Ternyata kalau pakai Zed editor dan Lmstudio setup-nya sangat Mudah!

## Install Lmstudio

Buka [https://lmstudio.ai](https://lmstudio.ai) dan install sesuai OS yang
digunakan, kebetulan saya pakai Fedora 41, jadi pilih Install snapimage untuk
Linux.

![Lmstudio](/static/r_Screenshot_20250202_105600.png)

Pastikan centang Add lms, LMS Studio CLI yang nanti diperlukan untuk running server-nya di background

## Install Model di Lmstudio

Buka aplikasi desktop Lmstudio dan arahkan pointer ke menu Discover di sebelah kiri (gambar Lup)

![Download dan Install Model](/static/r_Screenshot_20250202_110623.png)

Saya menggunakan model Qwen 2.5 Coder 1.5b (qwen2.5-coder-1.5b-instruct), karena
dari hasil trial dan error yang saya lakukan, model ini yang bisa jalan di PC
saya.

## Jalankan API server Lmstudio

Karena diawal kita sudah centang untuk add lms ke system `PATH` maka seharusnya tinggal execute perintah dibawah ini.

```bash
jimbo@slim7:~$ lms server start
Starting server...
Success! Server is now running on port 1234
```

kalau lms belum bisa di eksekusi coba tambahkan lokasi ke lms binary ke system `PATH`
```bash
# Added by LM Studio CLI (lms)
export PATH="$PATH:$HOME/.lmstudio/bin"
```

Kalau api server sudah jalan kamu juga bisa monitoring statusnya lewat aplikasi
desktop-nya. Di menu bagian kiri gambar Console

![Pastikan API sudah berjalan](/static/r_Screenshot_20250202_120909.png)

## Konfigurasi Assistant Panel di Zed editor

Buka Zed editor, dan klik menu Assistant Panel di pojok kanan, akan terbuka
sidebar dan di Hamburger menu-nya klik Configure.

![Assistant Panel di Zed editor](/static/r_Screenshot_20250202_110758.png)

Pastikan Zed editor sudah terkoneksi dengan Lmstudio

![Zed editor terkoneksi dengan Lmstudio](/static/r_Screenshot_20250202_110949.png)

Nah terakhir pada bagian konfigurasi model tinggal pilih Model yang sebelumnya
kita gunakan di Lmstudio, yaitu `qwen2.5-coder-1.5b-instruct`

![Pilih model](/static/r_Screenshot_20250202_111114.png)

selanjutnya ya kamu tinggal interaksi dengan Copilot Chat gratisan ini, sejauh
ini lumayan akurat untuk membantu saya mengerjakan project React Typescript dan
Golang, namun kadang-kadang memang ngaco tapi tidak masalah karena untuk hal
yang gratis menurut saya ini sudah cukup powerful.

![Copilot chat gratis](/static/r_Screenshot_20250202_111603.png)

## Kesimpulan

Kombinasi Zed editor dan LMStudio, kita sebagai developer bisa dapetin alternatif
gratis untuk Github Copilot yang berjalan secara lokal di komputer. Meski tidak
sempurna, solusi ini menawarkan coding assistant yang cukup powerful tanpa
batasan penggunaan dan biaya.
