# Menggunakan Split Keyboard

Ini adalah keyboard _open source_ bernama Redox Keyboard&nbsp;[^fn:1] yang didisain oleh Mattia Dal Ben, seorang master dalam bidang _Electrical Engineering_ dengan spesialisasi _Computer Science_ di _Universitas Udine_. Salah satu alasanku ingin bikin split keyboard karena bisa difungsikan seperti gambar diatas, karena sering kali ketika mengerjakan sesuatu didepan komputer, aku perlu mencatat beberapa hal, sedangkan ruang dimeja terbatas. Beberapa hari menggunakan keyboard ini belum terlalu terbiasa, hasil test kecepatan mengetik di keybr.com&nbsp;[^fn:2] hanya 22-27 wpm wqwq, tapi menurutku ini cuma masalah waktu sampai nanti bakal terbiasa.

![Penampakan keyboard](/static/20210429_075710_jhst9F.png)

Kelebihan keyboard semacam ini yang ditenagai arduino pro micro, aku bisa melakukan mapping sesuai keinginan dengan VIA&nbsp;[^fn:3]. Tentunya sebelum bisa dikonfigurasi melalui VIA harus flash firmwarenya dulu, karena ini split keyboard, firmwarenya diflash 2 kali, masing masing di bagian yang kiri dan kanan. Sebagai pecinta Vim binding (saat ini sudah jarang menggunakan vim, tapi menggunakan bindingnya dibeberapa aplikasi lain seperti tmux), saya juga register leader key tmux yang terdiri dari `Ctrl` + `A` + `Shift`, menjadi 1 tombol, menarik bukan?

![Konfigurasi melalui aplikasi Via](/static/via-redox.png)

Kekurangan keyboard ini apa?, saat ini saya merasa case-nya masih terlalu tebal, ini dikarenakan juga karena di PCB-nya masih menggunakan housing untuk memasang promicro-nya. Jadi kalau penggunaan jangka panjang mungkin kurang nyaman ditangan, dan berpotensi RSI (Repetitive Strain Injury)&nbsp;[^fn:4].

## Catatan Kaki

[^fn:1]: [Redox Keyboard](https://braindump.fly.dev/notes/20210306084529-redox%5Fkeyboard/)
[^fn:2]: [Keybr.com](https://www.keybr.com)
[^fn:3]: [VIA](https://caniusevia.com)
[^fn:4]: [Notes on RSI for Developers](https://www.swyx.io/rsi-tips/)
