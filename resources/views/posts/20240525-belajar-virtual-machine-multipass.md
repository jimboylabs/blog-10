# Belajar Virtual Machine dengan Multipass

[TOC]

## Mengapa perlu Virtual Machine?

Eksplorasi terhadap sesuatu yang baru, lebih aman dilakukan di-lingkungan yang **terisolasi** terlebih dahulu. Misal dalam beberapa minggu kebelakang saya mengeksplorasi apakah [Podman](https://www.threads.net/@wayanjimmy/post/C6EIIuhBYJa) dapat menggantikan posisi Docker dalam cara kerja saya sehari-hari?

Akan sangat **riskan** jika saya ganti langsung Docker dengan Podman, melainkan saya bisa coba-coba dulu lewat VM (Virtual Machine), lakukan testing dengan beberapa kondisi yang saya perlukan, seperti:

- Apakah kompatibel dengan aplikasi lain yang bergantung pada Docker? dalam hal ini [Tilt.dev](https://tilt.dev)
- Apakah kompatibel dengan docker-compose ?
- Bagaimana cara menjalankan Kubernetes in Docker dengan Podman?

Jika bisa menjawab pertanyaan diatas, barulah rasa **Percaya diri** saya meningkat untuk dapat mengganti Docker dengan Podman, dan dalam proses-nya saya tidak perlu utak-atik konfigurasi Host machine saya, karena **stabilitas** Host Machine sangat penting untuk kelancaran kerja sehari-hari.

Melainkan yang saya obrak-abrik adalah VM-nya karena jika gagal tinggal destroy dan create lagi VM yang baru, lakukan beberapa **iterasi**, sampai saya bisa menjawab semua pertanyaan diatas.

## Kenapa Multipass?

Sebagai pengguna Ubuntu, [Multipass](https://braindump.fly.dev/notes/20210228151250-multipass/) sudah mencakup apa yang saya perlukan:

- Bikin VM dengan dasar Ubuntu
- Bisa install beberapa versi ubuntu 24.04, 20.04, dll
- Mulai tanpa konfigurasi

Dibandingkan dengan Vagrant, saya merasa Multipass adalah sebuah peningkatan!

## Cara membuat VM 

Untuk instalasi bisa cek langsung dari [dokumentasi-nya](https://multipass.run/install). Untuk membuat sebuah VM gunakan perintah dibawah.

```bash
multipass launch devbox
```

`devbox` adalah nama VM yang bisa kamu ganti, kamu juga bisa konfigurasi banyak CPU, Memory dan Storage yang dialokasikan untuk VM ini.

```bash
multipass launch -c 2 -d 30G -m 2G --name devbox 24.04
```

Perintah diatas artinya buat sebuah VM dengan 2 CPU, Storage 30 Giga, Memory 2 Giga, dengan nama devbox dan versi Ubuntu 24.04

## SSH ke dalam VM

Untuk bisa mulai ngoprek kedalam VM, kita mesti shh dulu

```bash
multipass shell devbox
```

Untuk memulai coba jalankan htop

```bash
htop
```

![Jalankan htop](/static/multipass-ubuntu-htop-small.png)

dari sini kamu akan mendapatkan informasi kalau CPU VM ini dibatasi 2 core, sesuai dengan spesifikasi yang kita buat kan?

kalau sudah dalam VM kita bisa mulai install aplikasi yang pengen kita oprek.

```bash
sudo apt update 
sudo apt upgrade
```

![Jalankan htop](/static/multipass-ubuntu-update-small.png)

Mari coba install podman dan jalankan container hello world.

```bash
sudo apt install -y podman podman-docker
```

![Jalankan Podman Hello World](/static/multipass-hello-podman-small.png)

## Hapus VM

Jika merasa sudah cukup bermain-main dengan VM, kamu bisa hapus, dan `purge` untuk melegakan kembali storage yang sebelum-nya di-claim oleh VM.

```
multipass delete devbox
multipass purge
```

## Kesimpulan

- VM adalah salah satu alat bantu yang bisa dimasukan ke dalam perlengkapan sehari-hari sebagai Pengembang Aplikasi.
- VM memberikan isolasi dan memungkinkan Pengembang untuk me-replikasi sebuah lingkungan, agar bisa semirip mungkin dengan kondisi ketika dijalankan dalam lingkungan Produksi.
- Multipass memberikan kemudahan bagi Pengembang yang sudah familiar dengan Ubuntu dan memerlukan bantuan alat berupa VM
