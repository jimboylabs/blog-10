# Linux Minipc untuk Remote Development

[TOC]

Tulisan ini adalah cerita bagaimana saya membuat Linux Ryzen Minipc sebagai alat bantu Development Jarak Jauh (Remote dev) saya menggunakan Laptop Windows sebagai mesin utama, namun hanya sebagai thin client yang mana terkoneksi dengan Minipc secara local dan global melalui jaringan VPN [Tailscale](https://tailscale.com/).

## Sedikit Konteks

![Dilema Laptop Kantor](/static/my-office-laptop.jpg)

Ada beberapa masalah utama, kenapa saya perlu sebuah mesin Linux tambahan dalam cara kerja ngoding sehari-hari.

1. Terlibat dalam sebuah proyek yang melibatkan beberapa web service yang saling berkomunikasi. Bisa menjalankan semua services yang kita kembangkan di local machine adalah sebuah kewajiban buat saya, agar lebih produktif, karena bisa melakukan _Inner Loop_&nbsp;[^fn:1] dahulu, sebelum dilanjut Outer Loop tim (di test QA, lanjut fix oleh Developer).

2. Komponen web services sangat bervariasi dari proyek legacy PHP, golang, database Mysql, Redis dan Elasticsearch. Jika menjalankan semua komponen tersebut, lebih nyaman lewat docker atau podman, dan tentunya Perlu RAM yang cukup, saat itu saya hanya bermodalkan Macbook Air 2017 dengan RAM 8GB (T_T).

3. Kadang kala ngoprek juga perlu melalui Virtual Machine. Kenapa ? agar bisa replikasi infrastruktur semirip mungkin dengan kondisi lingkungan production, atau ingin uji coba sebuah ide tanpa mengotori host machine. Dalam kasus ini saya menggunakan [Multipass](https://braindump.fly.dev/notes/20210228151250-multipass/), yang mana perlu tambahan RAM lagi dong.

https://twitter.com/AjeyGore/status/1772610018640105849

## Persiapan Hardware

Biaya yang saya keluarkan sekitar 8jt Rupiah, berikut adalah rincian-nya

1. Ryzen Mini PC [tokopedia](https://www.tokopedia.com/electronikjuragan/mini-pc-beelink-ser4-4800u-amd-ryzen-7-16-500gb-windows-11-home) harga 6jt
2. Upgrade RAM ke 64 GB [tokopedia](https://www.tokopedia.com/harajuku-gaming/team-elite-so-dimm-memory-notebook-32-gb-ddr4-3200-mhz-black) harga 2jt

Tentu kamu bisa menghemat lagi, saya memilih mini pc karena penasaran akan performa Ryzen 7 dengan 8 core 16 thread-nya, bentuk-nya mungil tidak makan banyak tempat (*^_^*), untuk spesifikasi yang lebih budget contek dari tweet dibawah.

https://twitter.com/garindrapp/status/1771013714067738991

## Persiapan Software

### Tailscale

Singkat-nya dua mesin yang terpisah jaringan, dapat saling berhubungan asalkan sama-sama terkoneksi dengan Tailscale.

![VPN dengan Tailscale](https://talks.rollingsayu.xyz/remote-dev-machine/assets/remote-dev-machine-diagram-DqTJQdBT.png)

Minipc saya lokasi-nya di Bali, dengan Internet Biznet Home, sedangkan saya saat ini tinggal di Jakarta, dan memungkinkan terkoneksi dengan Minipc di Bali, dengan mengakses ip address yang didapat dari Tailscale.

Jika belum pernah coba Tailscale, saya sangat rekomendasikan untuk dicoba, karena dalam hal ini, komponen inilah yang paling penting.

### Setup SSH config

Kamu bisa buat ssh config di Windows sama hal-nya dengan di linux, ini bakal memudahkan connect ke remote host lewat Vscode.
File ini perlu di-simpan di `~/.ssh/config` atau `C:\Users\<username>\.ssh\config` kalau di Windows, contoh-nya seperti ini.

```
Host ser4
    HostName 192.168.x.x
    User jimbo
```

maka selanjutnya jika perlu ssh ke minipc bisa dengan perintah `ssh ser4`. Kamu mungkin perlu setup ssh-keygen terlebih dahulu dan ssh-copy-id untuk menghindari memasukan password setiap waktu.

### Vscode

1. Install Remote SSH Extension
2. Ctrl + Shift + P pilih Remote-SSH: Connect to Host
3. Pilih nama Host sesuai ssh config
4. Buka folder project dan terminal
5. Jalankan services
6. Port Forward
7. Profit

### Mutagen.io 

https://twitter.com/Sirupsen/status/1719319556882083943

Ini hanya alternatif vscode remote ssh & port forward, kalau lagi pengen pakai Neovim, mutagen juga punya fitur manajemen port forwarding dan sync file antara remote dan local. Berikut contoh-nya, lebih lengkap-nya bisa cek di [dokumentasi](https://mutagen.io/)-nya.

```bash
# Sync file remote ke local directory
mutagen sync create \
    --name=myproject ./project \
    ser4:~/clones/project --ignore-vcs

# Port forward 
mutagen forward create \
    --name=ollama tcp:localhost:11434 \
    ser4:tcp:localhost:11434
```

### Tilt

Nah [Tilt](https://tilt.dev/), adalah semacam perekat yang menyambungkan beberapa komponen pendukung development diatas.

1. source code
2. tasks seperti proses compile atau build container image
3. service database yang jalan di docker compose 
4. deploy ke local Kubernetes cluster (untuk kasus saya)

dan webui-nya, menurut saya sungguh membantu untuk menampilkan log saat perlu debugging.

![Tilt webui](https://gitlab.com/jimboylabs/commerce-asynq-gotenberg/-/raw/main/screenshots/screenshot-asynq-2.png)

## Pengalaman Sejauh ini

Sangat puas!

1. Bisa install service macam-macam tanpa khawatir kekurangan RAM, Docker atau Podman, MySQL, Elasticsearch, Ollama, Local kubernetes dengan Kind, atau Microk8s semua di dalam Minipc dengan RAM 64GB tentu-nya (〜￣▽￣)〜
2. Inner loop untuk early feedback lebih sering, menambah rasa percaya diri, sebelum melempar kerjaan ke Outer loop
3. Hemat baterai laptop, karena beban development lebih banyak ditanggung oleh Minipc


## Catatan Kaki

[^fn:1]: [Developer Productivity Thoughts](https://sourcegraph.com/blog/developer-productivity-thoughts)
