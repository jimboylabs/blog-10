# Cara self host Turso LibSQL Server di Kubernetes

[TOC]

## Apa itu Libsql?

Aplikasi yang saya host baik di Homelab atau di [Fly.io](https://fly.io)
sebagian besar menggunakan sqlite sebagai database karena saya tidak ingin repot
setup database server. Setup saya sebelum-nya banyak menggunakan
[Litestream](https://github.com/benbjohnson/litestream) backup ke
[Cloudflare R2](https://www.cloudflare.com/developer-platform/products/r2/),
namun kekurangan-nya hanya 1 instance dari aplikasi kita yang bisa akses
database dalam 1 waktu, ini memang kekurangan dari sqlite.

[Libsql](https://github.com/tursodatabase/libsql) memberikan solusi sqlite lewat
http server yang memungkinkan lebih dari 1 instance server yang akses database
secara bersamaan. Misal aplikasi kita punya web _server_ dan _worker_ pada
container yang terpisah namun meng-akses database yang sama.

Itu kenapa saya tertarik ngoprek Libsql ini untuk dipasang di Homelab, apalagi
setelah Turso mengumumkan dukungan library Libsql client untuk Laravel, karena
dengan library [ini](https://x.com/tursodatabase/status/1877423735096983870)
saya tidak perlu repot berurusan dengan custom php extension.

## Membuat Statefulset

Untuk deploy libsql berikut adalah statefulset yang saya gunakan terdiri dari
container libsql-server dan pvc-nya.

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
    name: libsql-demo-1
    namespace: default
spec:
    serviceName: libsql-demo
    replicas: 1
    selector:
        matchLabels:
            app: libsql-demo
    template:
        metadata:
            labels:
                app: libsql-demo
        spec:
            containers:
                - name: libsql
                  image: ghcr.io/tursodatabase/libsql-server:v0.24.31
                  command: ["/bin/sqld"]
                  args:
                      - "--http-listen-addr"
                      - "127.0.0.1:8081"
                      - "--admin-listen-addr"
                      - "127.0.0.1:8080"
                      - "--enable-namespaces"
                  ports:
                      - containerPort: 8080
                      - containerPort: 8081
                  env:
                      - name: SQLD_NODE
                        value: primary
                  volumeMounts:
                      - name: data
                        mountPath: /var/lib/sqld
    volumeClaimTemplates:
        - metadata:
              name: data
          spec:
              accessModes: ["ReadWriteOnce"]
              storageClassName: openebs-hostpath
              resources:
                  requests:
                      storage: 512Mi
```

Dari entrypoint container libsql dapat dilihat kalau server expose beberapa port
antara lain

- 8080 untuk admin http server, berguna untuk manage namespaces
- 8081 untuk user http server, untuk di akses oleh client

disamping itu ada argument tambahan `--enable-namespaces` untuk enable
namespace, yang memungkinkan instance server ini bisa me-manage beberapa
database sekaligus.

## Membuat Namespace

Jika server sudah berjalan, kita bisa memuat beberapa namespace baru misalkan
setiap project kita memiliki database sendiri.

```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d {} \
     http://localhost:8080/v1/namespaces/db1/create
```

## Tes koneksi dengan Database

Sekarang kita bisa menyambungkan aplikasi dengan database db1 yang sudah dibuat.
Saya membuat script sederhana dengan Deno dan Drizzle ORM, code-nya bisa di
contoh di repo
[ini](https://gitlab.com/jimboylabs/try/-/tree/main/libsql-client?ref_type=heads).

Seperti dijelaskan disini, untuk akses db1 yang dimanage lewat namespace kita
perlu menggunakan custom host, agar gak ribet setup dns saya pakai
[nip.io](https://nip.io/).

```bash
export DATABASE_URL=http://db1.127-0-0-1.nip.io:8081
```

alamat diatas akan resolve ke 127.0.0.1 dan libsql akan mendapatkan nama
database `db1`, kemudian coba kita eksekusi database migration

```bash
jimbo@nobarapc:~/.../libsql-client$ deno -A --node-modules-dir npm:drizzle-kit migrate
No config path provided, using default 'drizzle.config.ts'
Reading config file '/home/jimbo/clones/labs/try/libsql-client/drizzle.config.ts'
[✓] migrations applied successfully
```

Migrate berhasil dan coba kita lakukan select data sederhana dari table yang
sudah dibuat

```bash
jimbo@nobarapc:~/.../libsql-client$ deno run -A src/main.ts
{ result: [] }
```

## Akses melalui DBeaver

Jika perlu akses database yang ada di libsql melalui GUI client salah satu
aplikasi yang disarankan dari turso adalah [DBeaver](https://dbeaver.io/).

![Akses dari DBeaver](/static/Screenshot_20250111_195657.png)

Jadi sekian sesi ngoprek Libsql kali ini, server ini akan saya pakai untuk dev
dan testing, sedangkan kalau production akan pakai dari
[Turso](https://turso.tech/) platform.
