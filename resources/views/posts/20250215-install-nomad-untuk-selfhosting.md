# Install Nomad, Podman dan Traefik untuk Selfhosting Rumahan

[TOC]

## Latar Belakang

[Nomad](https://www.nomadproject.io/) adalah sebuah _scheduler_ yang dikembangkan
oleh [Hashicorp](https://www.hashicorp.com), yang mana scheduler ini bisa
digunakan untuk melakukan scheduling _container_, _raw execution_, dan aplikasi lainnya.
Keunikan ini membuat Nomad bisa digunakan untuk menjalankan berbagai jenis
aplikasi, tidak hanya container lewat konsep [Task Driver](https://developer.hashicorp.com/nomad/docs/drivers).

## Yang perlu disiapkan

### Prasyarat Pengetahuan

- Pemahaman dasar tentang containerisasi (Docker, Podman, dll)
- Pengetahuan dasar  tentang Load balancer (Traefik)

### Lingkungan yang dibutuhkan

Jadi yang perlu disiapkan antara lain: 
- Host server dengan OS Ubuntu 21.04 
- [hashi-up](https://github.com/jsiebens/hashi-up) - CLI tool untuk mempermudah
proses instalasi

### Catatan Penting

- Instalasi akan dilakukan dalam mode _single-node_
- Server Nomad akan berfungsi ganda:
    - Sebagai server
    - Sebagai client untuk menjalankan workload
- Mode ini dipilih untuk mengurangi keperluan sumber daya

## Instalasi

Pastikan server tujuan sudah bisa diakses lewat ssh key. Kemudian jalankan perintah berikut:

```bash
hashi-up nomad install \
  --ssh-target-addr 192.168.1.10 \
  --ssh-target-user ubuntu \
  --ssh-target-key ~/.ssh/id_ed25519 \
  --server
```

tolong pastikan untuk mengganti IP, user dan alamat ssh key sesuai dengan
lingkunganmu.

## Podman Task Driver

Podman adalah salah satu task driver yang didukung Nomad, kenapa memilih Podman?
karena belakangan saya memang menggunakan Podman sebagai daily driver. Proses
instalasi dapat mengikuti panduan dari dokumentasi resmi Nomad di halaman
[ini](https://developer.hashicorp.com/nomad/plugins/drivers/podman#installation).

Selain itu, pastikan juga Podman sudah terinstal.

```bash
sudo apt install podman
```

Selanjutnya perlu dipastikan agar Nomad mengenali plugin ini.

```bash
sudo vim /etc/nomad.d/nomad.hcl
```

tambahkan baris berikut.

```hcl
# generated with hashi-up

datacenter = "dc1"
data_dir   = "/opt/nomad"
plugin_dir = "/opt/nomad/data/plugins" # [tl! ++]
server {
  enabled          = true
  bootstrap_expect = 1
}
```

Restart server Nomad.

```bash
sudo systemctl restart nomad
```

Pastikan plugin sudah tertera pada bagian task driver.

```bash
nomad node status -self -short | grep Drivers
CSI Drivers     = <none>
Drivers         = exec,podman
```

## Nomad Single Node

Untuk menjalankan Nomad sebagai _single-node_, kita perlu enable client mode
pada server Nomad ini. Edit lagi file `/etc/nomad.d/nomad.hcl` dan tambahkan
baris berikut

```hcl
# generated with hashi-up

datacenter = "dc1"
data_dir   = "/opt/nomad"
plugin_dir = "/opt/nomad/data/plugins"
server {
  enabled          = true
  bootstrap_expect = 1
}

client {            # [tl! ++]
    enabled = true  # [tl! ++]
}                   # [tl! ++]
```

Jangan lupa restart service nomad

## Traefik dan Nomad service discovery

Buka nomad webui melalui browser, biasanya di `http://192.168.1.10:4646/ui`. Kemudian
klik tombol "Run job" dan tambahkan spesifikasi job berikut.

```hcl
job "traefik" {
  datacenters = ["dc1"]
  type        = "service"
  
  group "traefik" {
    count = 1

    network {
      port "http" {
        static = 8080
      }

      port "admin" {
        static = 8081
      }
    }

    service {
      name     = "traefik-http"
      provider = "nomad"
      port     = "http"
    }

    task "server" {
      driver = "podman"

      config {
        image = "docker.io/traefik:v2.11.20"
        ports = ["admin", "http"]

        args = [
          "--api.dashboard=true",
          "--api.insecure=true", # Traefik akan diinstall dengan mode insecure, sebaiknya jangan di ekspose ke internet
          "--entrypoints.web.address=:${NOMAD_PORT_http}",
          "--entrypoints.traefik.address=:${NOMAD_PORT_admin}",
          "--providers.nomad=true",
          "--providers.nomad.endpoint.address=http://${NOMAD_IP_http}:4646",
          "--providers.nomad.exposedByDefault=false"
        ]
      }
    }
  }
}
```

## Deploy demo aplikasi

Selanjutnya kita akan deploy sebuah aplikasi yang mendemokan load balancing
sederhana melalui traefik yang sudah terkoneksi dengan Nomad service discovery.

```hcl
job "demo-webapp" {
  datacenters = ["dc1"]

  group "demo" {
    count = 3

    network {
      port  "http"{
        to = -1
      }
    }

    service {
      name = "demo-webapp"
      port = "http"
      provider = "nomad"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.demo-webapp-http.rule=Host(`demo-webapp-192-168-1-10.nip.io`)",
        "traefik.http.routers.demo-webapp-http.tls=false",
      ]

      check {
        type     = "http"
        path     = "/"
        interval = "2s"
        timeout  = "2s"
      }
    }

    task "server" {
      env {
        PORT    = "${NOMAD_PORT_http}"
        NODE_IP = "${NOMAD_IP_http}"
      }

      driver = "podman"

      config {
        image = "docker.io/hashicorp/demo-webapp-lb-guide"
        ports = ["http"]
      }
    }
  }
}
```

Setalah job di atas dijalankan, maka aplikasi `demo-webapp` akan melalui proses
_Load balancing_ oleh Traefik terlebih dahulu, setelah itu diarahkan ke
masing-masing container.

```
ubuntu@nomad01:~$ curl http://demo-webapp-192-168-1-10.nip.io:8080
Welcome! You are on node 192.168.1.10:20190
ubuntu@nomad01:~$ curl http://demo-webapp-192-168-1-10.nip.io:8080
Welcome! You are on node 192.168.1.10:25458
ubuntu@nomad01:~$ curl http://demo-webapp-192-168-1-10.nip.io:8080
Welcome! You are on node 192.168.1.10:25482
ubuntu@nomad01:~$ curl http://demo-webapp-192-168-1-10.nip.io:8080
Welcome! You are on node 192.168.1.10:20190
```

Dapat kamu lihat kalau yang meresponse request kita ada _beberapa container_
dengan masing-masing port yang _berbeda_, port tersebut dialokasikan oleh Nomad
secara dinamis.

Demikian tutorial singkat ini, di artikel selanjutnya saya akan bahas cara
menjalankan aplikasi _stateful_ semacam _database_ dengan _host volume_ di Nomad.
