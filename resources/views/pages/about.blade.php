<x-layout>
    <x-slot:title>About</x-slot:title>

    <article class="prose prose-gray">
        @markdown
        # About.me

        Web developer yg tinggal di Jakarta.
        Saat ini lagi seneng ngoprek Laravel, Go, dan Homelab.
        Tertarik juga ngobrol seputar komputer, kehidupan dan anime.

        Kabari saja kalau mau ngobrol, sambil ngopi-ngopi (*￣3￣)╭
        @endmarkdown
    </article>
</x-layout>
