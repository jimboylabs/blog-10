<?php

use Illuminate\View\View;

use function Laravel\Folio\render;

render(function (View $view) {
    $links = [
        ['url' => 'http://rezhajulio.id/?ref=blog.wayanjimmy.xyz', 'hostname' => 'rezhajulio.id'],
        ['url' => 'https://rizaumami.github.io/?ref=blog.wayanjimmy.xyz', 'hostname' => 'rizaumami.github.io'],
        ['url' => 'https://agung1402.wordpress.com/?ref=blog.wayanjimmy.xyz', 'hostname' => 'agung1402.wordpress.com'],
        ['url' => 'https://bluemeda.web.id/?ref=blog.wayanjimmy.xyz', 'hostname' => 'bluemeda.web.id'],
        ['url' => 'https://farahclara.id/?ref=blog.wayanjimmy.xyz', 'hostname' => 'farahclara.id'],
        ['url' => 'https://prehistoric.me/?ref=blog.wayanjimmy.xyz', 'hostname' => 'prehistoric.me'],
        ['url' => 'https://blog.arsmp.com/?ref=blog.wayanjimmy.xyz', 'hostname' => 'blog.arsmp.com'],
        ['url' => 'https://www.sumarsono.com/?ref=blog.wayanjimmy.xyz', 'hostname' => 'sumarsono.com'],
        ['url' => 'https://yogayudistira.id/?ref=blog.wayanjimmy.xyz', 'hostname' => 'yogayudistira.id'],
        ['url' => 'https://ahmadiham.id/?ref=blog.wayanjimmy.xyz', 'hostname' => 'ahmadiham.id'],
        ['url' => 'https://rizkiepratama.net/?ref=blog.wayanjimmy.xyz', 'hostname' => 'rizkiepratama.net'],
        ['url' => 'https://nsetyo.me/?ref=blog.wayanjimmy.xyz', 'hostname' => 'nsetyo.me'],
        ['url' => 'https://ha.hn.web.id/?ref=blog.wayanjimmy.xyz', 'hostname' => 'ha.hn.web.id'],
        ['url' => 'https://muhammadrefa.wordpress.com/?ref=blog.wayanjimmy.xyz', 'hostname' => 'muhammadrefa.wordpress.com'],
        ['url' => 'https://syamsu.dev/?ref=blog.wayanjimmy.xyz', 'hostname' => 'syamsu.dev'],
        ['url' => 'https://situsali.com/?ref=blog.wayanjimmy.xyz', 'hostname' => 'situsali.com'],
        ['url' => 'https://radhitya.org/?ref=blog.wayanjimmy.xyz', 'hostname' => 'radhitya.org'],
        ['url' => 'https://kresna.me/?ref=blog.wayanjimmy.xyz', 'hostname' => 'kresna.me'],
    ];

    return $view->with('links', $links);
}); ?>

<x-layout>
    <x-slot:title>Blogroll</x-slot:title>

    <article class="prose prose-gray">
        @markdown
        # Blogroll

        Kumpulan blog teman-teman:
        @endmarkdown

        <ul class="p-0 list-none">
            @foreach ($links as $link)
            <li class="p-0 transition-colors rounded-lg hover:bg-gray-50">
                <a href="{{ $link['url'] }}" target="_blank" rel="noopener" class="flex flex-col no-underline">
                    <span class="text-lg font-medium text-gray-900">{{ $link['hostname'] }}</span>
                </a>
            </li>
            @endforeach
        </ul>
    </article>
</x-layout>
