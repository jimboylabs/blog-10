<x-layout>
    <article class="prose prose-gray">
        @markdown($post->content)
    </article>
    <script src="https://utteranc.es/client.js"
            repo="wayanjimmy/comments"
            issue-term="pathname"
            label="blog"
            theme="github-light"
            crossorigin="anonymous"
            async>
    </script>
</x-layout>
