<?php

use App\Models\Post;
use Illuminate\View\View;

use function Laravel\Folio\render;

render(function (View $view) {
    $posts = Post::all();
    return $view->with('posts', $posts);
}); ?>

<x-layout>
    <x-slot:title>Home</x-slot>

        @if($posts->count() > 0)
        <section class="my-6">
            <h1 class="font-bold mb-2">Artikel</h1>
            <ul class="flex flex-col space-y-1">
                @foreach ($posts as $post)
                <li>
                    <a href="/posts/{{ $post->slug }}" class="hover:border-b-2 hover:border-gray-600 hover:cursor-pointer">
                        <time datetime="{{ $post->date }}" class="font-mono">{{ $post->date->format('d/m/Y') }}</time> {{ $post->title }}
                    </a>
                </li>
                @endforeach
            </ul>
        </section>
        @endif
</x-layout>
