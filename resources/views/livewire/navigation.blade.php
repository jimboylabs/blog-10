<?php

use function Livewire\Volt\{state};

//

?>

<nav class="container px-2 py-2 mx-auto center-v lg:w-1/2 md:w-full lg:text-left">
    <ul class="vertical">
        <li>
            <a
            href="/"
            class="transition-colors duration-200 hover:text-gray-600"
            wire:navigate
            >
            ~/home
            </a>
        </li>
        <li>
            <a
            href="/about"
            class="transition-colors duration-200 hover:text-gray-600"
            wire:navigate
            >
            ~/about
            </a>
        </li>
        <li>
            <a
            href="/blogroll"
            class="transition-colors duration-200 hover:text-gray-600"
            wire:navigate
            >
            ~/blogroll
            </a>
        </li>

    </ul>
</nav>
