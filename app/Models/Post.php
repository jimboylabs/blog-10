<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

class Post extends Model implements Feedable
{
    use HasFactory, \Sushi\Sushi;

    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope(
            "order",
            fn($builder) => $builder
                ->where("draft", false)
                ->orderBy("date", "desc")
        );
    }

    protected $rows = [
        [
            "title" => "Merantau",
            "date" => "2020-12-04",
            "slug" => "20201204-merantau",
            "description" => "Merantau",
            "draft" => false,
        ],
        [
            "title" => "Menjalani Quarter (Half?) Life Crisis",
            "date" => "2020-12-05",
            "slug" => "20201205-menjalani-quarter-life-crisis",
            "description" => "Menjalani Quarter (Half?) Life Crisis",
            "draft" => false,
        ],
        [
            "title" => "Buku Ikigai",
            "date" => "2021-01-03",
            "slug" => "20210103-buku-ikigai",
            "description" => "Buku Ikigai",
            "draft" => false,
        ],
        [
            "title" => "Menggunakan Split Keyboard",
            "date" => "2021-04-26",
            "slug" => "20210426-menggunakan-split-keyboard",
            "description" => "Menggunakan Split Keyboard",
            "draft" => false,
        ],
        [
            "title" => "Ngoprek Twirp RPC",
            "date" => "2021-09-09",
            "slug" => "20210909-ngoprek-twirp-rpc",
            "description" => "Ngoprek Twirp RPC",
            "draft" => false,
        ],
        [
            "title" => "Linux Minipc untuk Remote development",
            "date" => "2024-05-16",
            "slug" => "20240516-linux-minipc-untuk-remote-dev",
            "description" => "Linux Minipc untuk Remote development",
            "draft" => false,
        ],
        [
            "title" => "Belajar Virtual Machine dengan Multipass",
            "date" => "2024-05-25",
            "slug" => "20240525-belajar-virtual-machine-multipass",
            "description" => "Belajar Virtual Machine dengan Multipass",
            "draft" => false,
        ],
        [
            "title" => "Cara self host Turso Libsql di Kubernetes",
            "date" => "2025-01-11",
            "slug" =>
                "20250111-cara-self-host-turso-libsql-server-di-kubernetes",
            "description" =>
                "Self host libsql di Homelab untuk keperluan development dan testing",
            "draft" => false,
        ],
        [
            "title" => "Copilot Chat Gratis dengan Zed editor dan Lmstudio",
            "date" => "2025-02-02",
            "slug" => "20250202-copilot-chat-gratis-zed-editor-lmstudio",
            "description" =>
                "Alternatif gratis Copilot Chat dengan Zed editor dan Lmstudio",
            "draft" => false,
        ],
        [
            "title" =>
                "Install Nomad, Podman dan Traefik untuk Selfhosting Rumahan",
            "date" => "2025-02-15",
            "slug" => "20250215-install-nomad-untuk-selfhosting",
            "description" =>
                "Install Nomad, Podman dan Traefik untuk Selfhosting Rumahan",
            "draft" => false,
        ],
    ];

    protected function year(): Attribute
    {
        return Attribute::make(
            get: function () {
                $year = $this->date->format("Y");

                if ($year < 2020) {
                    return "old";
                }

                return $year;
            }
        );
    }

    protected function content(): Attribute
    {
        return Attribute::make(
            get: function () {
                return Storage::disk("views")->get("posts/{$this->slug}.md");
            }
        );
    }

    public function toFeedItem(): FeedItem
    {
        return FeedItem::create()
            ->id($this->id)
            ->title($this->title)
            ->summary($this->description)
            ->updated($this->date)
            ->link(url("/posts/{$this->slug}"))
            ->authorName("Wayan Jimmy")
            ->authorEmail("jimmyeatcrab@gmail.com");
    }

    public static function getFeedItems()
    {
        return Post::all();
    }
    protected function casts(): array
    {
        return [
            "date" => "date",
        ];
    }
}
